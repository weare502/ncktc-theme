<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?> "> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php language_attributes(); ?> > <!--<![endif]-->
<head>
	<meta charset="<?php bloginfo('charset'); ?>">

	<title><?php wp_title('|', true, 'right'); ?></title>

	<!-- Mobile viewport optimized: j.mp/bplateviewport -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<!-- Favicon and Feed -->
	<link rel="shortcut icon" type="image/x-icon" href="<?php echo home_url(); ?>/favicon.ico">
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> Feed" href="<?php echo home_url(); ?>/feed/">
	<link rel="stylesheet" href="https://use.typekit.net/xdq1unx.css">
	
	<style>
		.issuuembed { width: 100% !important; height: 90vh !important; max-height: 40rem; } #wp-toolbar.quicklinks { float: none; }
	</style>
<?php wp_head(); ?>

<!-- SiteScout Retargeting -->
<script type="text/javascript">var ssaUrl = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'pixel.sitescout.com/iap/41f86be8f9b56000';new Image().src = ssaUrl;</script>
</head>

<body <?php body_class('antialiased'); ?> >

<?php if( get_field('show_notification_bar', 'options') == 'No' ) : ?>
	<div class="notification-bar-sticky">
		<div class="notification-close-button" aria-role="button" title="Close Button">&times;</div>
		<div class="notification-bar-sticky__content">
			<p><?= get_field('notification_text', 'options'); ?></p>
		</div>
	</div>
<?php endif; ?>

<?php if(!is_page_template("page-landing.php")){ ?>
	<?php do_action( 'tha_body_top' ); ?>
	
	<header id="main-header" class="row" role="navigation">
		<nav id="qlinks" class="quicklinks">
			<div class="link-wrapper">
				<div><!-- push right --></div>
				<div class="quicklinks__right">
					<a href="https://ncktc.ethinksites.com/">Moodle</a>
					<a href="http://mail.google.com/a/ncktc.edu">Email</a>
					<a href="/cams/">TechKNOW</a>
					<a href="<?php echo get_permalink(375); ?>">Contact Us</a>
					<?php get_search_form(); ?>
				</div>
			</div>
		</nav>

		<div class="main-navigation">
			<a class="hide-mobile" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo get_stylesheet_directory_uri(); ?>/img/logo.png" alt="NCK Tech College Logo" /></a>
			<?php uberMenu_easyIntegrate(); ?>
			<span id="search-click-handler" class="fa fa-search"></span>
			<span class="searchbar-mobile"><?php get_search_form(); ?></span>
		</div>
	</header>

	<!-- Start the main container -->
	<div class="container" role="document"> <!-- tag closed in footer.php -->
		<div class="wrapper1">
			<?php if( is_front_page() ): ?>

				<section class="wit-header">
					<video id="wit-bg-video" class="wit-video" preload="none" autoplay="true" muted="true" loop="true">
						<source src="<?php echo get_stylesheet_directory_uri(); ?>/img/header-bg-video.mp4" type="video/mp4">
					</video>
					<img class="home-mobile-header" src="<?= get_stylesheet_directory_uri(); ?>/img/header-image.png" alt="Header Background Image">

					<div id="wit-video-container" class="wit-fade-container">
						<div class="rel-container">
							<video id="wit-video-inner" class="wit-fade-video" controls="controls" preload="none" poster="<?= get_field('header_video_poster')['url']; ?>">
								<source src="<?= get_field('header_video'); ?>" type="video/mp4">
							</video>
							<a class="close-video" href="javascript: void(0);">X</a>
						</div>
					</div>

					<div class="wit-header__overlay">
						<h1 class="home-h1"><?= get_field('large_header_text'); ?></h1>
						<p id="home-sub-header"><?= get_field('sub_header_text'); ?></p>
						<a class="play-button" href="javascript: void(0);">
							<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/play-icon.png" alt="Click to play video">
							<span style="padding-left: 5px;">Play Video</span>
						</a>

						<div class="wit-header__bottom-nav">
							<span class="wit-header__bottom-nav--box">
								<a href="/visit/">
									<?php include 'img/map-marker.svg'; ?>
									<span class="h5-heading">Schedule a Tour</span>
								</a>
							</span>

							<span class="skew-bar"></span>

							<span class="wit-header__bottom-nav--box">
								<a href="/events-main/">
									<?php include 'img/calendar-icon.svg'; ?>
									<span class="h5-heading">View Upcoming<br>Events</span>
								</a>
							</span>

							<span class="skew-bar"></span>
							
							<span class="wit-header__bottom-nav--box">
								<a class="last-tag" href="/contact/">
									<?php include 'img/phone-icon.svg'; ?>
									<span class=" h5-heading last-tag--text">Contact Us</span>
								</a>
							</span>
						</div> <!-- /.wrapper1__bottom-nav -->

					</div> <!-- /.[wit-header__overlay] -->
				</section>
				<img class="header-bottom-bar" src="<?php echo get_stylesheet_directory_uri(); ?>/img/bottom-header-bar.png" alt="Grey divider bar">
			<?php endif; ?>
		</div>
<?php }; ?>